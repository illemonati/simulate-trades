import pandas
from openpyxl import Workbook
from openpyxl import formatting, styles
from openpyxl.formatting.rule import ColorScaleRule, CellIsRule, FormulaRule

STARTING = 2000
STARTING_2 = 10000
PER_DAY = 1000
PER_DAY_PERCENT = 0.5
FEE = 250
OUTFILE = "./run1.xlsx"

red = '9c0103'
green = '006400'

red_font = styles.Font(color=red)
green_font = styles.Font(color=green)


def run_day(total, per_day, day_gain, adjustment_function=lambda x: x):
    if (total < per_day):
        raise Exception("Total Not Enough, gg")

    profit = per_day * day_gain
    profit = adjustment_function(profit)
    profit_percent_of_total = profit / total
    day_gain = profit / per_day
    return profit, profit_percent_of_total, day_gain


def generate_terminal(day, total, profit, profit_percent_of_total, total_gains):
    print(f"________Day {day}________")
    print(f"total: {total:.2f}")
    print(f"profit: {profit:.2f}")
    print(f"profit_percent_of_total: {profit_percent_of_total*100:.2f}%")
    print(f"total_gains: {total_gains*100:.2f}%")
    print()


def set_xl_width(sheet, *titles):
    for i, item in enumerate(titles):
        sheet.column_dimensions[f'{chr(i+65)}'].width = len(item) + 10


def write_xl(sheet, row, *items):
    for i, item in enumerate(items):
        sheet[f'{chr(i+65)}{row}'] = item


def write_row(sheet, row, *items):
    write_xl(sheet, row, *items)
    sheet[f"C{row}"].number_format = '0.00%'
    sheet[f"D{row}"].number_format = '0.00%'
    sheet[f"E{row}"].number_format = '0.00%'
    sheet[f"A{row}"].number_format = '"$ "#,##0.00'
    sheet[f"B{row}"].number_format = '"$ "#,##0.00'


def run(workbook, data, starting_total, sheet_name, per_day_method, adjustment_function=lambda x: x):
    total = starting_total
    sheet = workbook.create_sheet(sheet_name)
    titles = ["Total", "Profit", "Profit Percent Of Collateral",
              "Profit Percent Of Total", "Total Gains Percent"]
    write_xl(sheet, 1, *titles)
    set_xl_width(sheet, *titles)
    sheet.freeze_panes = sheet['F2']

    for i in range(len(data)):
        try:
            profit, profit_percent_of_total, day_gain = run_day(
                total, per_day_method(total), data[i], adjustment_function)
        except Exception as e:
            break
        total += profit
        total_gains = (total - starting_total) / starting_total
        generate_terminal(i+2, total, profit,
                          profit_percent_of_total, total_gains)
        write_row(sheet, i+2, total, profit, day_gain,
                  profit_percent_of_total, total_gains)

    for i in range(1, 5):
        sheet.conditional_formatting.add(f'{chr(i+65)}2:{chr(i+65)}{len(data)+2}', CellIsRule(
            operator='lessThan', formula=['0'], font=red_font))
        sheet.conditional_formatting.add(f'{chr(i+65)}2:{chr(i+65)}{len(data)+2}', CellIsRule(
            operator='greaterThan', formula=['0'], font=green_font))


def run_with_fixed_per_day(workbook, data, starting_total):
    run(workbook, data, starting_total,
        f"run with ${PER_DAY} per day, ${starting_total} total", lambda _: PER_DAY)


def run_with_percent_per_day(workbook, data, starting_total):
    run(workbook, data, starting_total,
        f"run with {PER_DAY_PERCENT * 100}% per day, ${starting_total} total", lambda total: total * PER_DAY_PERCENT)


def run_with_fixed_per_day_with_fee(workbook, data, starting_total):
    run(workbook, data, starting_total,
        f"fee run with ${PER_DAY} per day, ${starting_total} total", lambda _: PER_DAY, adjustment_function=lambda x: x - FEE/3)


def run_with_percent_per_day_with_fee(workbook, data, starting_total):
    run(workbook, data, starting_total,
        f"fee run with {PER_DAY_PERCENT * 100}% per day, ${starting_total} total", lambda total: total * PER_DAY_PERCENT, adjustment_function=lambda x: x - FEE/3)


def main():
    df = pandas.read_csv("data.csv")
    data = df.iloc[:, 0].tolist()
    workbook = Workbook()
    del workbook['Sheet']
    run_with_fixed_per_day(workbook, data, STARTING)
    run_with_percent_per_day(workbook, data, STARTING)
    run_with_fixed_per_day_with_fee(workbook, data, STARTING)
    run_with_percent_per_day_with_fee(workbook, data, STARTING)

    run_with_fixed_per_day(workbook, data, STARTING_2)
    run_with_percent_per_day(workbook, data, STARTING_2)
    run_with_fixed_per_day_with_fee(workbook, data, STARTING_2)
    run_with_percent_per_day_with_fee(workbook, data, STARTING_2)
    workbook.save(OUTFILE)


if __name__ == "__main__":
    main()
